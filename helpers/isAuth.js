export const getCookie = (cname, cookie) => {
  const name = cname + '='
  const decodedCookie = decodeURIComponent(cookie)
  const ca = decodedCookie.split(';')
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) === ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ''
}

export const checkCookie = (cname, cookie) => {
  const username = getCookie(cname, cookie)
  if (username === '') {
    return false
  }
  return true
}
