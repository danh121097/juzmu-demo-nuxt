import moment from 'moment'
export const convertDate = (date) => {
  return moment(date).locale('vi').startOf('seConds').fromNow()
}
