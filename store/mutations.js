const mutations = {
  isLogged(state, payload) {
    state.isLogin = payload.isCheckCookie
  },
  fetchPosts(state, payload) {
    state.posts = payload.posts
  },
  pushCommentToPost(state, payload) {
    state.posts[payload.indexPost].comments.push(payload.comment)
  },
  pushPost(state, payload) {
    state.posts.unshift(payload.post)
  },
}
export default mutations
