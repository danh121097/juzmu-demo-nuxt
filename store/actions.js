import { checkCookie } from '../helpers/isAuth'

const action = {
  nuxtServerInit({ commit }, { req }) {
    // eslint-disable-next-line no-console
    if (checkCookie('apollo-token', req.headers.cookie) && req.headers) {
      const isCheckCookie = checkCookie('apollo-token', req.headers.cookie)
      commit('isLogged', { isCheckCookie })
    }
  },
}
export default action
