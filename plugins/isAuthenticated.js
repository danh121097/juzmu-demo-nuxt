export default ({ app, redirect }) => {
  app.router.afterEach((to, from) => {
    const token = !!app.$apolloHelpers.getToken()
    const publicRoute = ['/register']
    if (!token && !publicRoute.includes(to.path)) {
      return redirect('/login')
    }
  })
}
